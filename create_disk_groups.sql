create diskgroup cutecrocodile external redundancy disk 
'/u01/cutecrocodile/cutecrocodile0',
'/u01/cutecrocodile/cutecrocodile1',
'/u01/cutecrocodile/cutecrocodile2',
'/u01/cutecrocodile/cutecrocodile3'
ATTRIBUTE 'COMPATIBLE.ASM'='11.2.0.0.0';

create diskgroup famousfly external redundancy disk 
'/u01/famousfly/famousfly0',
'/u01/famousfly/famousfly1',
'/u01/famousfly/famousfly2',
'/u01/famousfly/famousfly3',
'/u01/famousfly/famousfly4',
'/u01/famousfly/famousfly5'
ATTRIBUTE 'COMPATIBLE.ASM'='11.2.0.0.0';

create diskgroup bravefish external redundancy disk 
'/u01/bravefish/bravefish0',
'/u01/bravefish/bravefish1',
'/u01/bravefish/bravefish2'
ATTRIBUTE 'COMPATIBLE.ASM'='11.2.0.0.0';

create diskgroup cleverdog external redundancy disk 
'/u01/cleverdog/cleverdog0',
'/u01/cleverdog/cleverdog1',
'/u01/cleverdog/cleverdog2'
ATTRIBUTE 'COMPATIBLE.ASM'='11.2.0.0.0';

create diskgroup youngsquirrel external redundancy disk 
'/u01/youngsquirrel/youngsquirrel0',
'/u01/youngsquirrel/youngsquirrel1',
'/u01/youngsquirrel/youngsquirrel2',
'/u01/youngsquirrel/youngsquirrel3'
ATTRIBUTE 'COMPATIBLE.ASM'='11.2.0.0.0';