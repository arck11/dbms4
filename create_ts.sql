create tablespace ASM_TS datafile '+BRAVEFISH'
size 10m autoextend on next 100m
extent management local
segment space management auto;

DROP DISKGROUP CUTECROCODILE INCLUDING CONTENTS;
DROP DISKGROUP bravefish INCLUDING CONTENTS;
DROP DISKGROUP cleverdog INCLUDING CONTENTS;
DROP DISKGROUP famousfly INCLUDING CONTENTS;