/u01/l4/app.sh create_disk_group_files youngsquirrel 4 50m

asmcmd dsset '/u01/cutecrocodile/*,/u01/famousfly/*,/u01/bravefish/*,/u01/cleverdog/*,/u01/youngsquirrel/*';

echo "create diskgroup youngsquirrel external redundancy disk '/u01/youngsquirrel/youngsquirrel0', '/u01/youngsquirrel/youngsquirrel1', '/u01/youngsquirrel/youngsquirrel2', '/u01/youngsquirrel/youngsquirrel3' ATTRIBUTE 'AU_SIZE'='1M';" | sqlplus / as sysasm;