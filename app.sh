#!/usr/bin/env bash
FUNCTION=
if [ ! -z $1 ]; then
    FUNCTION="$1"
fi


stat_cssd() {
    crsctl stat resource ora.cssd -t
}

start_cssd() {
    crsctl start resource ora.cssd
}

stop_cssd() {
    crsctl stop resource ora.cssd
}

create_disk_group_files() {
    local name=$1;
    local disks=$2;
    local size=$3;
    mkdir -p /u01/$name;
    for ((i=0; i<disks ; i++)); do
        /usr/sbin/mkfile -n $size /u01/$name/$name$i && echo "file /u01/$name/$name$i created";
    done ;
    chown -R oracle:dba /u01/$name;
}

create_files() {
    create_disk_group_files "cutecrocodile" 4 100m
    create_disk_group_files "famousfly" 6 100m
    create_disk_group_files "bravefish" 3 100m
    create_disk_group_files "cleverdog" 3 500m
}

rm_disk_group() {
    local name=$1;
    rm -rf /u01/$name/ && echo "$name removed";
}

rm_files() {
    rm_disk_group "cutecrocodile"
    rm_disk_group "famousfly"
    rm_disk_group "bravefish"
    rm_disk_group "cleverdog"
    rm_disk_group "youngsquirrel"
}

ls_files() {
    ls -lh "/u01/cutecrocodile/" "/u01/famousfly/" "/u01/bravefish/" "/u01/cleverdog/";
}

case "$1" in
-h|--help)
    show-help
    ;;
*)
    if [ ! -z $(type -t $FUNCTION | grep function) ]; then
        $1 $2 $3 $4
    else
        show-help
    fi
esac