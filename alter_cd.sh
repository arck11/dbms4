#!/usr/bin/env bash

alter_clever_dog() {
    local id=$1
    echo $id;
    /usr/sbin/mkfile -n 30m /u01/cleverdog/cleverdog$id && echo "file /u01/cleverdog/cleverdog$id created";
    chown -R oracle:dba "/u01/cleverdog/cleverdog$id";
    echo "alter diskgroup cleverdog add disk '/u01/cleverdog/cleverdog$id' name CLEVERDOG_000$id;" | sqlplus / as sysasm && echo "CLEVERDOG_000$id disk added to cleverdog";
}

if [ -z "$1" ]; then
    alter_clever_dog 3
else
    alter_clever_dog $1
fi