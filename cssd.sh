#!/usr/bin/env bash
FUNCTION=
if [ ! -z $1 ]; then
    FUNCTION="$1"
fi

stat() {
    crsctl stat resource ora.cssd -t
}

start() {
    crsctl start resource ora.cssd
}

stop() {
    crsctl stop resource ora.cssd
}

case "$1" in
-h|--help)
    show-help
    ;;
*)
    if [ ! -z $(type -t $FUNCTION | grep function) ]; then
        $1 $2 $3 $4
    else
        show-help
    fi
esac